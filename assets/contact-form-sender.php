﻿<?php
/**
 * This example shows settings to use when sending over SMTP with TLS and custom connection options.
 */
//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/src/Exception.php';
require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';

date_default_timezone_set('Etc/UTC');

//Create a new PHPMailer instance
$mail = new PHPMailer(true);
try {
//Set locale to Turkish
$mail->setLanguage('tr', 'phpmailer/language/');
//Mail UTF-8 charset
$mail->CharSet = 'UTF-8';
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Auth type
//$mail->AuthType = "LOGIN";
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//$mail->SMTPDebug = true;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.yandex.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 465;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'ssl';
//Custom connection options
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = 'info@completomedia.com';
//Password to use for SMTP authentication
$mail->Password = "smbruiwmkhqefeqt"; //"ventacan2017*";
//Set who the message is to be sent from
$mail->setFrom('info@completomedia.com', 'Exengaz Info mail');
//Set an alternative reply-to address
$mail->addReplyTo('info@completomedia.com', 'Exengaz Info mail');
//Set who the message is to be sent to
$mail->AddBCC('exen@exengaz.com.tr', 'Exengaz form mail');
//Add To, CC and BCC
//$mail->addAddress('test@test.com', 'testTo');
//$mail->AddCC('test@test.com', 'testCC');
//$mail->AddBCC('test@test.com', 'testBCC');
//Set the subject line
$mail->Subject = 'exengaz.com.tr web sitesi iletişim formu';
//send as HTML
$mail->IsHTML(true);
//Take data into variables
$name = $_POST['cfName'];
$surname = $_POST['cfSurname'];
$email = $_POST['cfEmail'];
$phone = $_POST['cfPhone'];
$message = $_POST['cfMessage'];
//Put the variables into mail body
$mail->Body = '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body>
		<table cellpadding="5" cellspacing="0" border="1" width="640" style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">

	    	<tr>
	    		<td colspan="3" align="center"><strong style="font-size:20px">exengaz.com.tr web sitesi iletişim formu</strong></td>
	    	<tr>
	    	<tr>
	    		<th width="120" align="left">İsim</th>
	    		<td width="20" align="center">:</td>
	    		<td>'.$name.'</td>
	    	<tr>
	    	<tr>
	    		<th align="left">Soyisim</th>
	    		<td align="center">:</td>
	    		<td>'.$surname.'</td>
	    	<tr>
	    	<tr>
	    		<th align="left">E-posta</th>
	    		<td align="center">:</td>
	    		<td>'.$email.'</td>
	    	<tr>
	    	<tr>
	    		<th align="left">Telefon numarası</th>
	    		<td align="center">:</td>
	    		<td>'.$phone.'</td>
	    	<tr>
	    	<tr>
	    		<th align="left">Mesajı</th>
	    		<td align="center">:</td>
	    		<td>'.$message.'</td>
	    	<tr>
    	</table>
	</body>
</html>';

$mail->send();

echo 'Message has been sent';
} catch (Exception $e) {
    echo "Mailer Error: " . $mail->ErrorInfo;
}

?>