/*
* @author Serhan Karakaya <http://serhankarakaya.com>
* @link https://completodigital.com
*/

//Google Analytics
/*var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-17067393-1']);
_gaq.push(['_trackPageview']);
(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();*/

//Homepage slider
$(window).load(function() {
    $('#mpslider').flexslider({
        useCSS: false,
        animation: "fade",
        slideshow: true,
        animationLoop: true,
        slideshowSpeed: 6000,
        animationSpeed: 700,
        pauseOnAction: false,
        pauseOnHover: false,
        directionNav: false,
        controlNav: true
    });
});

$(window).on("load resize",function(){
    $("#mpslider").css({
        height: windowHeight,
        width: windowWidth
    });
});

//Cookie reminder
var crInjectFunction = function(){
    var cookieReminder = document.createElement("div");
    var link = document.createElement("a");
    var closeButton = document.createElement("button");

    cookieReminder.className = "cr-content";
    cookieReminder.innerHTML = "Web sitemizden en iyi şekilde yararlanmanız için çerez kullanılmaktadır. Bu web sitesine giriş yaparak çerez kullanmayı kabul etmiş sayılırsınız. "
    link.innerHTML = "Daha fazla bilgi için tıklayınız.";
    closeButton.className = "cr-closebutton";

    closeButton.addEventListener( 'click', function(){
        var timeAsDays = 1000*60*60*24*3; // 3 days
        var expires = new Date((new Date()).valueOf() + timeAsDays);
        document.cookie = "crReminder=true;expires=" + expires.toUTCString();
        $(".cr-content").hide();
    });

    link.setAttribute("target", "_blank");
    link.setAttribute("href", "assets/files/exengaz-cerez-politikasi.pdf");

    cookieReminder.appendChild(link);
    cookieReminder.appendChild(closeButton);

    document.body.appendChild(cookieReminder);
}

$(document).on("ready",function(){
    if (document.cookie.indexOf('crReminder=true') == -1) {
        crInjectFunction();
    }
});

//Subnav copy into select form small screens
$("ul.subnav").each(function(count, item){
    var childrens = $(item).children();
    var menuArray = [];

    childrens.each(function(childrensCount, childrenItem){
        var childrenItemLink = $(childrenItem).children();
        var isSelected = childrenItemLink.hasClass("active");
        var isBlank = false;
        if (childrenItemLink.attr('target') === '_blank') {
            isBlank = true;
        }
        var href = childrenItemLink.attr('href');
        var text = childrenItemLink.text();

        var childrenItemData = {
            'isSelected': isSelected,
            'isBlank': isBlank,
            'href': href,
            'text': text
        };

        menuArray.push(childrenItemData);
    });

    var select = document.createElement("select");
    select.className = "subnavselect form-control";

    $.each(menuArray, function (index, value) {
        //select.append('<option value="'+value["href"]+'">'+value["text"]+'</option>')
        var optionItem = document.createElement("option");
        optionItem.innerHTML = value["text"];
        optionItem.setAttribute("value", value["href"]);
        if(value["isSelected"] === true) {
            optionItem.setAttribute("selected", "");
        }
        if(value["isBlank"] === true) {
            optionItem.setAttribute("blankwindow", 'true');
        }
        select.add(optionItem)
    });

    $(select).insertAfter(item);
});

//Subnavselect onchange functions
$('.subnavselect').on('change',function(event){
    var checked = $(this).find(":checked");

    var value = checked.val();
    var isBlank = checked.attr('blankwindow');

    if(isBlank === 'true') {
        window.open(value,'_blank');
    } else {
        window.location = value;
    }
});

//Contact form validation
$('form#contact-form').validate({
    rules: {
        cfName: {
            required: true,
            rangelength: [2, 30]
        },
        cfSurname: {
            required: true,
            rangelength: [2, 30]
        },
        cfEmail: {
            required: true,
            email: true
        },
        cfPhone: {
            required: true,
            rangelength: [8, 25]
        },
        cfMessage: {
            required: true,
            rangelength: [10, 500]
        }
    },
    messages: {
        cfName: {
            required: "Lütfen adınızı giriniz.",
            rangelength: "Adınız minimum 2 maksimum 30 karakterden oluşmalıdır."
        },
        cfSurname: {
            required: "Lütfen soyadınızı giriniz.",
            rangelength: "Soyadınız minimum 2 maksimum 30 karakterden oluşmalıdır."
        },
        cfEmail: {
            required: "Lütfen mail adresinizi giriniz.",
            email: "Lütfen uygun bir mail adresi giriniz."
        },
        cfPhone: {
            required: "Lütfen numaranızı giriniz.",
            rangelength: "Telefon numaranız minimum 8 maksimum 25 karakterden oluşmalıdır."
        },
        cfMessage: {
            required: "Lütfen mesajınızı giriniz.",
            rangelength: "Mesajınız minimum 10 maksimum 500 karakterden oluşmalıdır."
        }
    },
    submitHandler: function (formData) {
        var form = $('form#contact-form');

        $.post("assets/contact-form-sender.php", form.serialize())
            .done(function() {
                form.trigger("reset");
                window.alert("Form başarı ile gönderildi.");
            })
            .fail(function() {
                window.alert("Teknik bir nedenden dolayı form gönderilemedi. Lütfen daha sonra tekrar deneyiniz.");
            })
            .always(function() {
                $("form#contact-form button[type=submit]").css({
                    'display': 'inline-block'
                });
            });
    }
});

//Become autogas dealer form validation
$('form#become-autogas-dealer-form').validate({
    rules: {
        badfName: {
            required: true,
            rangelength: [2, 30]
        },
        badfSurname: {
            required: true,
            rangelength: [2, 30]
        },
        badfEmail: {
            required: true,
            email: true
        },
        badfPhone: {
            required: true,
            rangelength: [8, 25]
        },
        badfCity: {
            required: true
        },
        badfHasStation: {
            required: true
        },
        badfMessage: {
            required: true,
            rangelength: [10, 500]
        }
    },
    messages: {
        badfName: {
            required: "Lütfen adınızı giriniz.",
            rangelength: "Adınız minimum 2 maksimum 30 karakterden oluşmalıdır."
        },
        badfSurname: {
            required: "Lütfen soyadınızı giriniz.",
            rangelength: "Soyadınız minimum 2 maksimum 30 karakterden oluşmalıdır."
        },
        badfEmail: {
            required: "Lütfen mail adresinizi giriniz.",
            email: "Lütfen uygun bir mail adresi giriniz."
        },
        badfPhone: {
            required: "Lütfen numaranızı giriniz.",
            rangelength: "Telefon numaranız minimum 8 maksimum 25 karakterden oluşmalıdır."
        },
        badfCity: {
            required: "Lütfen şehri seçiniz."
        },
        badfHasStation: {
            required: "Lütfen bir seçim yapınız."
        },
        badfMessage: {
            required: "Lütfen mesajınızı giriniz.",
            rangelength: "Mesajınız minimum 10 maksimum 500 karakterden oluşmalıdır."
        }
    },
    submitHandler: function (formData) {
        var form = $('form#become-autogas-dealer-form');

        $.post("assets/become-autogas-dealer-form-sender.php", form.serialize())
            .done(function() {
                form.trigger("reset");
                window.alert("Form başarı ile gönderildi.");
            })
            .fail(function() {
                window.alert("Teknik bir nedenden dolayı form gönderilemedi. Lütfen daha sonra tekrar deneyiniz.");
            })
            .always(function() {
                $("form#become-autogas-dealer-form button[type=submit]").css({
                    'display': 'inline-block'
                });
            });
    }
});

//Become tubegas dealer form validation
$('form#become-tubegas-dealer-form').validate({
    rules: {
        btdfName: {
            required: true,
            rangelength: [2, 30]
        },
        btdfSurname: {
            required: true,
            rangelength: [2, 30]
        },
        btdfEmail: {
            required: true,
            email: true
        },
        btdfPhone: {
            required: true,
            rangelength: [8, 25]
        },
        btdfCity: {
            required: true
        },
        btdfMessage: {
            required: true,
            rangelength: [10, 500]
        }
    },
    messages: {
        btdfName: {
            required: "Lütfen adınızı giriniz.",
            rangelength: "Adınız minimum 2 maksimum 30 karakterden oluşmalıdır."
        },
        btdfSurname: {
            required: "Lütfen soyadınızı giriniz.",
            rangelength: "Soyadınız minimum 2 maksimum 30 karakterden oluşmalıdır."
        },
        btdfEmail: {
            required: "Lütfen mail adresinizi giriniz.",
            email: "Lütfen uygun bir mail adresi giriniz."
        },
        btdfPhone: {
            required: "Lütfen numaranızı giriniz.",
            rangelength: "Telefon numaranız minimum 8 maksimum 25 karakterden oluşmalıdır."
        },
        btdfCity: {
            required: "Lütfen şehri seçiniz."
        },
        btdfMessage: {
            required: "Lütfen mesajınızı giriniz.",
            rangelength: "Mesajınız minimum 10 maksimum 500 karakterden oluşmalıdır."
        }
    },
    submitHandler: function (formData) {
        var form = $('form#become-tubegas-dealer-form');

        $.post("assets/become-tubegas-dealer-form-sender.php", form.serialize())
            .done(function() {
                form.trigger("reset");
                window.alert("Form başarı ile gönderildi.");
            })
            .fail(function() {
                window.alert("Teknik bir nedenden dolayı form gönderilemedi. Lütfen daha sonra tekrar deneyiniz.");
            })
            .always(function() {
                $("form#become-tubegas-dealer-form button[type=submit]").css({
                    'display': 'inline-block'
                });
            });
    }
});

//Json data
var gdc = $("select#gasDealers-city");
var gdd = $("select#gasDealers-district");
var tgdc = $("select#tubeGasDealers-city");
var tgdd = $("select#tubeGasDealers-district");
var gpc = $("select#gasPrices-city");
var tgpc = $("select#tubeGasPrices-city");
var tgs = $("select#tubeGasSize");
var dealersList = $("ul#dealers-list");
var gasPricesContainer = $("#gas-prices-container");
var citiesData = {
    cities: [],
    gasDealerCities: [],
    tubeGasDealerCities: [],
    cityParts: [],
    tubeCityParts: []
};
var pullDataEvent = $.getJSON("assets/js/data.json",function(){
}).fail(function(){
    console.info("Json datası çekilirken bir hata oluştu.");
}).done(function(data){
    citiesData.cities = data;
    $.each(citiesData.cities, function(i, city){
        if(city.gasDealers){
            //if city has gas dealers
            citiesData.gasDealerCities.push(city.name);
            var gasDealerUniqueDistricts = [];
            $.each(city.gasDealers, function(i, dealer){
                gasDealerUniqueDistricts.push(dealer.district)
            });
            gasDealerUniqueDistricts = _.uniq(gasDealerUniqueDistricts);
            city.gasDealerUniqueDistricts = gasDealerUniqueDistricts;
        }

        if(city.tubeDealers){
            //if city has tube gas dealers
            citiesData.tubeGasDealerCities.push(city.name);
            var tubeGasDealerUniqueDistricts = [];
            $.each(city.tubeDealers, function(i, dealer){
                tubeGasDealerUniqueDistricts.push(dealer.district)
            });
            tubeGasDealerUniqueDistricts = _.uniq(tubeGasDealerUniqueDistricts);
            city.tubeGasDealerUniqueDistricts = tubeGasDealerUniqueDistricts;
        }

        if(city.cityParts) {
            //Parts for gas
            $.each(city.cityParts, function(i, cityPart){
                var name = city.name;
                if(cityPart.name) {
                    name = name + ' ('+ cityPart.name+')'
                }
                citiesData.cityParts.push({
                    cpName: name,
                    pumpPrice: cityPart.pumpPrice
                });
            });

            //Parts for tube
            $.each(city.cityParts, function(i, cityPart){
                if (_.isArray(cityPart.tube)) {
                    var name = city.name;
                    if(cityPart.name) {
                        name = name + ' ('+ cityPart.name+')'
                    }
                    citiesData.tubeCityParts.push({
                        cpName: name,
                        tube: cityPart.tube
                    });
                }
            });
        }
    });

    var gasPricesCityPartsOptions = [];
    $.each(citiesData.cityParts, function(key, val){
        gasPricesCityPartsOptions.push( "<option value='" + val.cpName + "'>" + val.cpName + "</option>");
    });
    gpc.append(gasPricesCityPartsOptions.join(""));

    var tubeGasPricesCityPartsOptions = [];
    $.each(citiesData.tubeCityParts, function(key, val){
        tubeGasPricesCityPartsOptions.push( "<option value='" + val.cpName + "'>" + val.cpName + "</option>");
    });
    tgpc.append(tubeGasPricesCityPartsOptions.join(""));

    var gasDealerCitiesOptions = [];
    $.each(citiesData.gasDealerCities, function(key, val){
        gasDealerCitiesOptions.push( "<option value='" + val + "'>" + val + "</option>");
    });
    gdc.append(gasDealerCitiesOptions.join(""));

    var tubeGasDealerCitiesOptions = [];
    $.each(citiesData.tubeGasDealerCities, function(key, val){
        tubeGasDealerCitiesOptions.push( "<option value='" + val + "'>" + val + "</option>");
    });
    tgdc.append(tubeGasDealerCitiesOptions.join(""));
});

//Remove all values from the select except the first one and select the empty valued children
var removeAllExceptFirst = function(item){
    item.find('option').not(':first').remove();
    item.val("").change();
};

var formatGasData = function(gasDealersData) {
    var dataContainer = [];
    if(gasDealersData && _.isArray(gasDealersData) && gasDealersData.length > 0) {
        $.each(gasDealersData, function(i, dealer) {
            var phoneBlock = '';
            var comma = ""
            $.each(dealer.phoneNumbers, function(i, phoneNumber) {
                if (i > 0) {
                    comma = ","
                } else {
                    comma = " "
                }
                phoneNumber = '0' + phoneNumber;
                phoneBlock = phoneBlock + comma +'<a href="tel:'+ phoneNumber +'">' +phoneNumber +'</a>';
            });

            dataContainer.push('<li><h5>'+ dealer.name +'</h5><p>'+ dealer.address +' '+ dealer.district +'</p><p><strong class="text-dark">Tel:</strong>'+ phoneBlock +'</p></li>');
        });
    }

    return dataContainer;
};

gdc.on("change", function(){
    var cityValue = gdc.find( "option:checked").val();
    if(cityValue){
        gdd.removeClass('d-none');
        var gasDealerDistrictsOptions = [];
        $.each(citiesData.cities, function(i, city){
            if(city.name == cityValue){
                $.each(city.gasDealerUniqueDistricts, function(key, val){
                    gasDealerDistrictsOptions.push("<option value='" + val + "'>" + val + "</option>");
                });
                removeAllExceptFirst(gdd);
                gdd.append(gasDealerDistrictsOptions.join(""));
            }
        });
    } else {
        dealersList.empty();
        gdd.addClass('d-none');
    }
});
gdd.on("change", function(){
    var cityValue = gdc.find( "option:checked").val();
    var districtValue = gdd.find("option:checked").val();
    var updatedData = [];
    if(districtValue){
        $.each(citiesData.cities, function(i, city){
            if(city.name == cityValue){
                updatedData = _.filter(city.gasDealers,function(dealerDet){return dealerDet.district == districtValue;});
            }
        });
    }
    dealersList.empty();
    dealersList.append(formatGasData(updatedData).join(""));
});

gpc.on("change", function(){
    var cityValue = gpc.find("option:checked").val();
    var updatedText = "";

    $.each(citiesData.cityParts, function(key, val){
        if(val.cpName == cityValue) {
            updatedText = '<strong class="text-dark">'+ val.pumpPrice +'</strong>'+ ' TL / Litre'
        }
    });

    gasPricesContainer.empty();
    gasPricesContainer.append(updatedText);

    //If economic autogas page
    if(cityValue,$("input#fiyat_otogaz").length > 0 ) {
        $.each(citiesData.cityParts, function(key, val){
            if(val.cpName == cityValue) {
                calculateMechanism();
                $("input#fiyat_otogaz").val(val.pumpPrice).change();
            }
        });
    } else {
        $("input#fiyat_otogaz").val(0).change();
    }
});

tgdc.on("change", function(){
    var cityValue = tgdc.find( "option:checked").val();
    if(cityValue){
        tgdd.removeClass('d-none');
        var tubeGasDealerDistrictsOptions = [];
        $.each(citiesData.cities, function(i, city){
            if(city.name == cityValue){
                $.each(city.tubeGasDealerUniqueDistricts, function(key, val){
                    tubeGasDealerDistrictsOptions.push("<option value='" + val + "'>" + val + "</option>");
                });
                removeAllExceptFirst(tgdd);
                tgdd.append(tubeGasDealerDistrictsOptions.join(""));
            }
        });
    } else {
        dealersList.empty();
        tgdd.addClass('d-none');
    }
});
tgdd.on("change", function(){
    var cityValue = tgdc.find( "option:checked").val();
    var districtValue = tgdd.find("option:checked").val();
    var updatedData = [];
    if(districtValue){
        $.each(citiesData.cities, function(i, city){
            if(city.name == cityValue){
                updatedData = _.filter(city.tubeDealers,function(dealerDet){
                    return dealerDet.district == districtValue;
                });
            }
        });
    }

    dealersList.empty();
    dealersList.append(formatGasData(updatedData).join(""));
});

tgpc.on("change", function(){
    gasPricesContainer.empty();
});
tgs.on("change", function(){
    var cityValue = tgpc.find( "option:checked").val();
    var sizeValue = tgs.find( "option:checked").val();
    var updatedText = "";

    if (tgpc) {
        $.each(citiesData.tubeCityParts, function(key, val){
            if(val.cpName == cityValue) {

                $.each(val.tube, function(i, tube){
                    if(tube.prodWeightByKg == sizeValue){
                        updatedText = '<strong class="text-dark">'+ tube.price +'</strong>'+ ' TL'
                    }
                });
            }
        });

        gasPricesContainer.empty();
        gasPricesContainer.append(updatedText);
    }
});


var closestDataUpdate = function(element) {
    var value = element.value;
    var output = element.$element[0].parentNode.getElementsByTagName('output')[0] || element.$element[0].parentNode.parentNode.getElementsByTagName('output')[0];
    output.innerHTML = value
}

var calculateMechanism = function() {
    var kmPerYear = $("#yilda_yaklasik_yapilan_kmZ").val();
    var gasolinePerHunderKm = $("#100km_benzin_tuketimW").val();
    var gasolinePricePerLiter = $("#guncel_benzin_fiyatY").val();
    var autoGasPricePerLiter = $("#fiyat_otogaz").val();
    var calculations = {
        yearlyGasolineExpense: 0,
        yearlyAutoGasExpense: 0,
        yearlyProfit: 0
    }

    if(kmPerYear && gasolinePerHunderKm && gasolinePricePerLiter && autoGasPricePerLiter) {
        calculations = {
            yearlyGasolineExpense: Math.floor(gasolinePricePerLiter * kmPerYear / gasolinePerHunderKm),
            yearlyAutoGasExpense: Math.floor(autoGasPricePerLiter * kmPerYear / gasolinePerHunderKm * 1.3),
            yearlyProfit: Math.floor((gasolinePricePerLiter * kmPerYear / gasolinePerHunderKm) - (autoGasPricePerLiter * kmPerYear / gasolinePerHunderKm * 1.3))
        };
    }

    $('#totalProfit').text(calculations.yearlyProfit + ' TL');

    return calculations;
}

//Range slider implementation
$('input[type=range]').rangeslider({
    // Deactivate the feature detection
    polyfill: false,

    // Callback function
    onInit: function() {
        closestDataUpdate(this);
    },
    onSlide: function(){
        closestDataUpdate(this);
    },
    onSlideEnd: function(position,value) {
        closestDataUpdate(this);
        calculateMechanism();
    }
});


//İletişim bölge select eventleri
var networkVal;
$('#network-select').on('change', function () {
    var val = $(this).val();

    networkVal = val;

    $('.contact-address-areas, .area-info').addClass('hidden');
    $('#'+val).removeClass('hidden');
});
$('.contact-address-areas select').on('change', function () {
    var val = $(this).val();
    $('.area-info').addClass('hidden');
    $('#'+networkVal+'-'+val).removeClass('hidden');
    console.log('#'+networkVal+'-'+val);
});
